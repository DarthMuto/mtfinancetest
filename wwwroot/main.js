(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-template #buttons>\n  <div class=\"mb-3\">\n    <button class=\"btn btn-info mr-3\" (click)=\"refresh()\">Refresh</button>\n    <button class=\"btn btn-info mr-3\" (click)=\"createNew()\">Add new virtual server</button>\n    <button class=\"btn btn-danger\" (click)=\"removeSelected()\" [class.disabled]=\"haveNothingToRemove()\">Remove selected</button>\n  </div>\n</ng-template>\n\n<table class=\"table table-bordered table-striped mb-3 mt-3\">\n  <tr>\n    <th>Current date:</th>\n    <td>{{currentDateTime | isoDateTime}}</td>\n  </tr>\n  <tr>\n    <th>Total usage time:</th>\n    <td>{{totalUsageTime | timeSpan}}</td>\n  </tr>\n</table>\n\n<ng-template [ngTemplateOutlet]=\"buttons\"></ng-template>\n\n<table class=\"table table-striped table-bordered mb-3\">\n  <tr>\n    <th>Virtual server id</th>\n    <th>Created at</th>\n    <th>Removed at</th>\n    <th>Selected for remove</th>\n  </tr>\n  <tr *ngFor=\"let virtualServer of virtualServers\">\n    <td>{{virtualServer.virtualServerId}}</td>\n    <td>{{virtualServer.createDateTime | isoDateTime}}</td>\n    <td>{{virtualServer.removeDateTime | isoDateTime}}</td>\n    <td>\n      <div class=\"btn-group btn-group-toggle\" *ngIf=\"!virtualServer.removeDateTime\">\n        <label class=\"btn-danger\" ngbButtonLabel>\n          <input type=\"checkbox\" ngbButton [(ngModel)]=\"virtualServer.selected\">\n          <span *ngIf=\"!virtualServer.selected\">Remove</span>\n          <span *ngIf=\"virtualServer.selected\">Cancel remove</span>\n        </label>\n      </div>\n    </td>\n  </tr>\n</table>\n\n<ng-template [ngTemplateOutlet]=\"buttons\"></ng-template>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_source_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data-source.service */ "./src/app/data-source.service.ts");
/* harmony import */ var _models_virtual_server__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./models/virtual-server */ "./src/app/models/virtual-server.ts");




var AppComponent = /** @class */ (function () {
    function AppComponent(dataSource) {
        var _this = this;
        this.dataSource = dataSource;
        this.virtualServers = [];
        this.totalUsageTime = null;
        this.currentDateTime = null;
        this.dataSource.virtualServersState.subscribe(function (newState) {
            _this.currentDateTime = new Date(Date.parse(newState.currentDateTime.toString()));
            _this.virtualServers = newState.virtualServers.map(function (s) { return Object.assign(new _models_virtual_server__WEBPACK_IMPORTED_MODULE_3__["VirtualServer"](), s); });
            _this.totalUsageTime = newState.totalUsageTime;
            _this.totalUsageTimeIncrementing = newState.totalUsageTimeIncrementing;
        });
        window.setInterval(function () {
            _this.currentDateTime = new Date(_this.currentDateTime.valueOf() + 1000);
            if (_this.totalUsageTimeIncrementing) {
                var totalUsageDateTime = new Date(Date.parse('0001-01-01 ' + _this.totalUsageTime) + 1000);
                var h = totalUsageDateTime.getHours();
                var m = totalUsageDateTime.getMinutes();
                var s = totalUsageDateTime.getSeconds();
                _this.totalUsageTime = (h < 10 ? '0' + h : h)
                    + ':' + (m < 10 ? '0' + m : m)
                    + ':' + (s < 10 ? '0' + s : s);
            }
        }, 1000);
        this.dataSource.refresh();
    }
    AppComponent.prototype.createNew = function () {
        this.dataSource.createNew();
    };
    AppComponent.prototype.removeSelected = function () {
        if (this.haveNothingToRemove()) {
            return;
        }
        this.dataSource.remove(this.virtualServers.filter(function (vs) { return vs.selected; }).map(function (vs) { return vs.virtualServerId; }));
    };
    AppComponent.prototype.refresh = function () {
        this.dataSource.refresh();
    };
    AppComponent.prototype.haveNothingToRemove = function () {
        return this.virtualServers.filter(function (vs) { return vs.selected; }).length === 0;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_data_source_service__WEBPACK_IMPORTED_MODULE_2__["DataSourceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _iso_date_time_pipe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./iso-date-time.pipe */ "./src/app/iso-date-time.pipe.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _time_span_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./time-span.pipe */ "./src/app/time-span.pipe.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _iso_date_time_pipe__WEBPACK_IMPORTED_MODULE_5__["IsoDateTimePipe"],
                _time_span_pipe__WEBPACK_IMPORTED_MODULE_7__["TimeSpanPipe"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/data-source.service.ts":
/*!****************************************!*\
  !*** ./src/app/data-source.service.ts ***!
  \****************************************/
/*! exports provided: DataSourceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataSourceService", function() { return DataSourceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var DataSourceService = /** @class */ (function () {
    function DataSourceService(httpClient) {
        this.httpClient = httpClient;
        // private endpointUrl = 'http://localhost:5000/api/virtual-servers';
        this.endpointUrl = '/api/virtual-servers';
        this.virtualServersState = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    DataSourceService.prototype.nextState = function (state) {
        this.virtualServersState.next(state);
    };
    DataSourceService.prototype.refresh = function () {
        var _this = this;
        this.httpClient
            .get(this.endpointUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (e) { return _this.catchError(e); }))
            .subscribe(function (s) { return _this.nextState(s); });
    };
    DataSourceService.prototype.createNew = function () {
        var _this = this;
        this.httpClient
            .post(this.endpointUrl, '')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (e) { return _this.catchError(e); }))
            .subscribe(function (s) { return _this.nextState(s); });
    };
    DataSourceService.prototype.remove = function (idsToRemove) {
        var _this = this;
        this.httpClient
            .post(this.endpointUrl + '/remove', { idsToRemove: idsToRemove })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (e) { return _this.catchError(e); }))
            .subscribe(function (s) { return _this.nextState(s); });
    };
    DataSourceService.prototype.catchError = function (err) {
        var msg = 'Failed to connect to ' + this.endpointUrl;
        alert(msg);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(msg);
    };
    DataSourceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], DataSourceService);
    return DataSourceService;
}());



/***/ }),

/***/ "./src/app/iso-date-time.pipe.ts":
/*!***************************************!*\
  !*** ./src/app/iso-date-time.pipe.ts ***!
  \***************************************/
/*! exports provided: IsoDateTimePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsoDateTimePipe", function() { return IsoDateTimePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var IsoDateTimePipe = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](IsoDateTimePipe, _super);
    function IsoDateTimePipe() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IsoDateTimePipe.prototype.transform = function (value, args) {
        return _super.prototype.transform.call(this, value, 'y-MM-dd HH:mm:ss');
    };
    IsoDateTimePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'isoDateTime'
        })
    ], IsoDateTimePipe);
    return IsoDateTimePipe;
}(_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]));



/***/ }),

/***/ "./src/app/models/virtual-server.ts":
/*!******************************************!*\
  !*** ./src/app/models/virtual-server.ts ***!
  \******************************************/
/*! exports provided: VirtualServer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualServer", function() { return VirtualServer; });
var VirtualServer = /** @class */ (function () {
    function VirtualServer() {
        this.selected = false;
    }
    return VirtualServer;
}());



/***/ }),

/***/ "./src/app/time-span.pipe.ts":
/*!***********************************!*\
  !*** ./src/app/time-span.pipe.ts ***!
  \***********************************/
/*! exports provided: TimeSpanPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeSpanPipe", function() { return TimeSpanPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TimeSpanPipe = /** @class */ (function () {
    function TimeSpanPipe() {
    }
    TimeSpanPipe.prototype.transform = function (value, args) {
        if (value === null) {
            return '';
        }
        var valueArr = value.split('.');
        if (valueArr.length > 1) {
            valueArr.pop();
        }
        return valueArr.join('.');
    };
    TimeSpanPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'timeSpan'
        })
    ], TimeSpanPipe);
    return TimeSpanPipe;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/muto/Projects/MtFinanceTestFrontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map