﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MtFinanceTest.Entries;
using MtFinanceTest.Infrastructure;

namespace MtFinanceTest.Controllers
{
    [Route("api/virtual-servers")]
    [ApiController]
    public class VirtualServersController : ControllerBase
    {
        private readonly DbContext _dbContext;

        public VirtualServersController(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public VirtualServersSummary GetSummary()
        {
            return new VirtualServersSummary
            {
                VirtualServers = _dbContext.VirtualServers.OrderBy(vs => vs.VirtualServerId).ToList(),
                CurrentDateTime = DateTime.Now
            };
        }

        [HttpPost]
        public VirtualServersSummary CreateVirtualServer()
        {
            _dbContext.VirtualServers.Add(new VirtualServer());
            _dbContext.SaveChanges();
            return GetSummary();
        }

        [HttpPost("remove")]
        public VirtualServersSummary RemoveSelected(RemoveVirtualServersRequest request)
        {
            _dbContext.VirtualServers
                .Where(vs => request.IdsToRemove.Contains(vs.VirtualServerId) && vs.RemoveDateTime == null)
                .ToList()
                .ForEach(vs => vs.RemoveDateTime = DateTime.Now);
            _dbContext.SaveChanges();
            return GetSummary();
        }
    }
}