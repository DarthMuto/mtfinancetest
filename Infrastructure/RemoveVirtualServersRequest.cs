using System.Collections.Generic;

namespace MtFinanceTest.Infrastructure
{
    public class RemoveVirtualServersRequest
    {
        public List<int> IdsToRemove { get; set; }
    }
}