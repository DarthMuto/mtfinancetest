using System;
using System.Collections.Generic;
using System.Linq;
using MtFinanceTest.Entries;

namespace MtFinanceTest.Infrastructure
{
    public class VirtualServersSummary
    {
        public DateTime CurrentDateTime { get; set; }
        public List<VirtualServer> VirtualServers { get; set; }

        public TimeSpan TotalUsageTime
        {
            get
            {
                var result = TimeSpan.Zero;

                // If lifespans of several VS intersect, they unify in a single `period`
                DateTime? periodStartDateTime = null;
                DateTime? periodEndDateTime = null;

                foreach (var virtualServer in VirtualServers.OrderBy(vs => vs.CreateDateTime))
                {
                    // If we're starting a new period
                    if (periodStartDateTime == null)
                        periodStartDateTime = virtualServer.CreateDateTime;

                    // If lifespan of current VS does not intersect with current period, flush current period to result
                    // and start a new one
                    if (periodEndDateTime != null && periodEndDateTime < virtualServer.CreateDateTime)
                    {
                        result += periodEndDateTime.Value - periodStartDateTime.Value;
                        periodStartDateTime = virtualServer.CreateDateTime;
                        periodEndDateTime = null;
                    }

                    // If there is a non-removed VS, no further checks needed - lifespan of still alive VS will include
                    // any lifespan of any VS started after current one
                    if (virtualServer.RemoveDateTime == null)
                    {
                        result += CurrentDateTime - periodStartDateTime.Value;
                        periodEndDateTime = null;
                        break;
                    }

                    if (periodEndDateTime == null || periodEndDateTime < virtualServer.RemoveDateTime)
                        periodEndDateTime = virtualServer.RemoveDateTime;
                }

                if (periodEndDateTime != null)
                    result += periodEndDateTime.Value - periodStartDateTime.Value;

                return result;
            }
        }

        public bool TotalUsageTimeIncrementing => VirtualServers.Any(vs => vs.RemoveDateTime == null);
    }
}