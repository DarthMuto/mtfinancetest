create table VirtualServers
(
	VirtualServerId int identity
		constraint PK_VirtualServers
			primary key,
	CreateDateTime datetime2 not null,
	RemoveDateTime datetime2
)
go

