using System;

namespace MtFinanceTest.Entries
{
    public class VirtualServer
    {
        public int VirtualServerId { get; set; }
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
        public DateTime? RemoveDateTime { get; set; }
    }
}