using Microsoft.EntityFrameworkCore;
using MtFinanceTest.Entries;

namespace MtFinanceTest
{
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<VirtualServer> VirtualServers { get; set; }
    }
}